﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WorkforceManager_API_Example
{
    public class API
    {

        private string m_guid = "";
        private string m_username = "";
        private string m_password = "";

        public API(string _guid, string _username, string _password)
        {
            this.m_guid = _guid;
            this.m_username = _username;
            this.m_password = _password;
        }

        #region points

        /// <summary>
        /// Returns all ‘Points’ related data, of an Account that where last updated since the requested _modifiedSince date, optionally we can request points that belong only to one or many specific ‘Point Groups’        
        /// </summary>
        public API_V1.wsGetPointsResponse GetAllPoints_v1(DateTime _modifiedSince, string[] _pointgroups)
        {
            try
            {
                API_V1.api_v1 api = new API_V1.api_v1();

                API_V1.wsGetPointsResponse resp =
                    api.GetAllPoints_v1(
                    _guid: this.m_guid,
                    _username: this.m_username,
                    _password: this.m_password,
                    _modifiedSince: _modifiedSince,
                    _pointGroups: _pointgroups
                    );
                if (resp == null) throw new Exception("resp == null");
                if (resp.resultCodeEnum != API_V1.resultCodeEnum.RETURN_OK) throw new Exception(JsonConvert.SerializeObject(resp));
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        /// <summary>
        /// Returns all ‘Points’ related data, of an Account that match any of the given field criteria    
        /// </summary>
        public API_V1.wsGetPointsResponse GetPoints_v1(string _customerid, string _name, string _tag1, string _tag2, string _tag3)
        {
            try
            {
                //Generate default values
                if (string.IsNullOrWhiteSpace(_customerid) == true) _customerid = "";
                if (string.IsNullOrWhiteSpace(_name) == true) _name = "";
                if (string.IsNullOrWhiteSpace(_tag1) == true) _tag1 = "";
                if (string.IsNullOrWhiteSpace(_tag2) == true) _tag2 = "";
                if (string.IsNullOrWhiteSpace(_tag3) == true) _tag3 = "";

                API_V1.api_v1 api = new API_V1.api_v1();

                API_V1.wsGetPointsResponse resp =
                    api.GetPoints_v1(
                    _guid: this.m_guid,
                    _username: this.m_username,
                    _password: this.m_password,
                    _customerid: _customerid,
                    _name: _name,
                    _tag1: _tag1,
                    _tag2: _tag2,
                    _tag3: _tag3
                    );
                if (resp == null) throw new Exception("resp == null");
                if (resp.resultCodeEnum != API_V1.resultCodeEnum.RETURN_OK) throw new Exception(JsonConvert.SerializeObject(resp));
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        /// <summary>
        /// Returns all ‘Point Groups’ of an Account as a string of ‘Point Group’ elements       
        /// </summary>
        public API_V1.wsGetPointGroupsResponse GetPointGroups_v1()
        {
            try
            {
                API_V1.api_v1 api = new API_V1.api_v1();

                API_V1.wsGetPointGroupsResponse resp =
                    api.GetPointGroups_v1(
                    _guid: this.m_guid,
                    _username: this.m_username,
                    _password: this.m_password
                    );
                if (resp == null) throw new Exception("resp == null");
                if (resp.resultCodeEnum != API_V1.resultCodeEnum.RETURN_OK) throw new Exception(JsonConvert.SerializeObject(resp));
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }


        /// <summary>
        /// Allows the Insert/Update of ‘Point’ Elements for an Account.
        /// </summary>
        public API_V1.wsGenericResponse AddPoints_v1(API_V1.wsAddPoints_Points_PointGroups_Users[] _wsAddPoints_Points_PointGroups_UsersList)
        {
            try
            {
                API_V1.api_v1 api = new API_V1.api_v1();

                API_V1.wsGenericResponse resp =
                    api.AddPoints_v1(
                    _guid: this.m_guid,
                    _username: this.m_username,
                    _password: this.m_password,
                    _wsAddPoints_Points_PointGroups_UsersList: _wsAddPoints_Points_PointGroups_UsersList
                    );
                if (resp == null) throw new Exception("resp == null");
                if (resp.resultCodeEnum != API_V1.resultCodeEnum.RETURN_OK) throw new Exception(JsonConvert.SerializeObject(resp));
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        #endregion

        #region users

        /// <summary>
        /// Returns all ‘Users’ of an Account as a string of ‘User’ elements including their Permissions.    
        /// </summary>
        public API_V1.wsGetUsersResponse GetUsers_v1()
        {
            try
            {
                API_V1.api_v1 api = new API_V1.api_v1();

                API_V1.wsGetUsersResponse resp =
                    api.GetUsers_v1(
                    _guid: this.m_guid,
                    _username: this.m_username,
                    _password: this.m_password
                    );
                if (resp == null) throw new Exception("resp == null");
                if (resp.resultCodeEnum != API_V1.resultCodeEnum.RETURN_OK) throw new Exception(JsonConvert.SerializeObject(resp));
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        /// <summary>
        /// Returns all ‘Users’ with ‘Worker Permissions’ of an Account as a string array of ‘username’ elements.    
        /// </summary>
        public API_V1.wsGetWorkerArrayResponse GetWorkerArray_v1()
        {
            try
            {
                API_V1.api_v1 api = new API_V1.api_v1();

                API_V1.wsGetWorkerArrayResponse resp =
                    api.GetWorkerArray_v1(
                    _guid: this.m_guid,
                    _username: this.m_username,
                    _password: this.m_password
                    );
                if (resp == null) throw new Exception("resp == null");
                if (resp.resultCodeEnum != API_V1.resultCodeEnum.RETURN_OK) throw new Exception(JsonConvert.SerializeObject(resp));
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        #endregion

        #region routes
        
        /// <summary>
        /// Returns all ‘Routes’ and related data, of an Account filtered by the requested _routeDate date.
        /// </summary>
        public API_V1.wsResponse GetRoutesForDate_v1(DateTime _routeDate)
        {
            try
            {
                API_V1.api_v1 api = new API_V1.api_v1();

                API_V1.wsResponse resp =
                    api.GetRoutesForDate_v1(
                    _guid: this.m_guid,
                    _username: this.m_username,
                    _password: this.m_password,
                    _routeDate: _routeDate
                    );
                if (resp == null) throw new Exception("resp == null");
                if (resp.resultCodeEnum != API_V1.resultCodeEnum.RETURN_OK) throw new Exception(JsonConvert.SerializeObject(resp));
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public API_V1.wsResponse_wsRouteVisitList GetUnassignedJobsForDate_v1(DateTime _routeDate)
        {
            try
            {
                API_V1.api_v1 api = new API_V1.api_v1();

                API_V1.wsResponse_wsRouteVisitList resp =
                    api.GetUnassignedJobsForDate_v1(
                    _guid: this.m_guid,
                    _username: this.m_username,
                    _password: this.m_password,
                    _routeDate: _routeDate
                    );
                if (resp == null) throw new Exception("resp == null");
                if (resp.resultCodeEnum != API_V1.resultCodeEnum.RETURN_OK) throw new Exception(JsonConvert.SerializeObject(resp));
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        /// <summary>
        /// Inserts or Updates ‘Routes’ and their child relations entities for an Account. Points can also be inserted from the same Api Call.
        /// </summary>
        public API_V1.wsResponse AddRoutesForDate_v3(
            DateTime _routeDate,
            API_V1.bridgeServiceModeEnum _bridgeServiceMode,
            bool _bUpdatePointLatLng,
            API_V1.wsRoute_v2[] _routelist
            )
        {
            try
            {
                API_V1.api_v1 api = new API_V1.api_v1();

                API_V1.wsResponse resp =
                    api.AddRoutesForDate_v3(
                    _guid: this.m_guid,
                    _username: this.m_username,
                    _password: this.m_password,
                    _routeDate : _routeDate,
                    _bridgeServiceMode: _bridgeServiceMode,
                    _bUpdatePointLatLng: _bUpdatePointLatLng,
                    _routelist: _routelist
                    );
                if (resp == null) throw new Exception("resp == null");
                if (resp.resultCodeEnum != API_V1.resultCodeEnum.RETURN_OK) throw new Exception(JsonConvert.SerializeObject(resp));
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
     
        #endregion
    }
}
