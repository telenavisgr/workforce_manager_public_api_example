﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using WorkforceManager_API_Example;
using WorkforceManager_API_Example.API_V1;
using System.Collections.Generic;

namespace WorkforceManager_API_Example_UT
{
    public static class Common_UnitTest
    {
        public static void Log(Object _obj, bool isJson = false)
        {
            if (isJson)
            {
                System.Diagnostics.Trace.WriteLine(_obj.ToString());
                return;
            }
            JsonSerializerSettings js = new JsonSerializerSettings();
            //js.DateTimeZoneHandling = DateTimeZoneHandling.Local;
            System.Diagnostics.Trace.WriteLine(JsonConvert.SerializeObject(_obj, js));
        }
    }

    public static class My
    {
        public static string m_guid = "01b62469-7cb5-4692-a69b-2ec198f474ec"; //Insert valid GUID
        public static string m_username = "test_user"; //Insert valid Username
        public static string m_password = "11111"; //Insert valid Password
    }

    [TestClass]
    public class Points
    {
        [TestMethod]
        public void GetAllPoints_v1()
        {
            try
            {
                API api = new API(
                    _guid: My.m_guid,
                    _username: My.m_username,
                    _password: My.m_password
                    );

                wsGetPointsResponse resp =
                     api.GetAllPoints_v1(
                     _modifiedSince: DateTime.Now.AddYears(-1),
                     _pointgroups: new string[] { }
                     );

                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.resultCodeEnum == resultCodeEnum.RETURN_OK);
                Common_UnitTest.Log(resp);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void GetPoints_v1()
        {
            try
            {
                API api = new API(
                    _guid: My.m_guid,
                    _username: My.m_username,
                    _password: My.m_password
                    );

                wsGetPointsResponse resp =
                    api.GetPoints_v1(
                    _customerid: "",
                    _name: "",
                    _tag1: "",
                    _tag2: "",
                    _tag3: ""
                    );

                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.resultCodeEnum == resultCodeEnum.RETURN_OK);
                Common_UnitTest.Log(resp);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void GetPointGroups_v1()
        {
            try
            {
                API api = new API(
                    _guid: My.m_guid,
                    _username: My.m_username,
                    _password: My.m_password
                    );

                wsGetPointGroupsResponse resp = api.GetPointGroups_v1();

                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.resultCodeEnum == resultCodeEnum.RETURN_OK);
                Common_UnitTest.Log(resp);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void AddPoints_v1()
        {
            try
            {
                API api = new API(
                    _guid: My.m_guid,
                    _username: My.m_username,
                    _password: My.m_password
                    );

                List<wsAddPoints_Points_PointGroups_Users> list = new List<wsAddPoints_Points_PointGroups_Users>();
                //► Create 3 points
                for (int i = 0; i < 3; i++) {
                    wsAddPoints_Points_PointGroups_Users item = new wsAddPoints_Points_PointGroups_Users();
               
                    item.isUpdatePointGroupRelationIfExist = false;
                    PointGroupToUsers pgu = new PointGroupToUsers(); //leave it empy no point group to users update

                    item.isUpdatePointLatLonIfExist = true;
                    Point p = new Point();                    
                    p.CustomerId =  "Test CustomerId " + i.ToString(); //
                    p.Name =  "Test Name " + i.ToString();
                    p.Address = "Test Address " + i.ToString();
                    p.Description = "Test Description " + i.ToString();
                    p.GeocodeX = 30.1;
                    p.GeocodeY = 30.2;
                    p.Tag1 = "Test Tag1 - " + i.ToString();
                    p.Tag2 = "Test Tag2 - " + i.ToString();
                    p.Tag3 = "Test Tag3 - " + i.ToString();
                    p.Tag4 = "Test Tag4 - " + i.ToString();
                    p.Tag5 = "Test Tag5 - " + i.ToString();
                    p.Tag6 = "Test Tag6 - " + i.ToString();
                    p.Tag7 = "Test Tag7 - " + i.ToString();
                    p.Tag8 = "Test Tag8 - " + i.ToString();
                    p.Tag9 = "Test Tag9 - " + i.ToString();
                    p.Tag10 = "Test Tag10 - " + i.ToString();
                    p.Visible = true;                        

                    item.point = p;
                    list.Add(item);
                }
                

                wsGenericResponse resp =
                    api.AddPoints_v1(
                        _wsAddPoints_Points_PointGroups_UsersList: list.ToArray()                
                    );

                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.resultCodeEnum == resultCodeEnum.RETURN_OK);
                Common_UnitTest.Log(resp);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }

    [TestClass]
    public class Users
    {
        [TestMethod]
        public void GetUsers_v1()
        {
            try
            {
                API api = new API(
                    _guid: My.m_guid,
                    _username: My.m_username,
                    _password: My.m_password
                    );

                wsGetUsersResponse resp = api.GetUsers_v1();

                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.resultCodeEnum == resultCodeEnum.RETURN_OK);
                Common_UnitTest.Log(resp);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void GetWorkerArray_v1()
        {
            try
            {
                API api = new API(
                    _guid: My.m_guid,
                    _username: My.m_username,
                    _password: My.m_password
                    );

                wsGetWorkerArrayResponse resp = api.GetWorkerArray_v1();

                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.resultCodeEnum == resultCodeEnum.RETURN_OK);
                Common_UnitTest.Log(resp);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

    }

    [TestClass]
    public class Routes
    {
        ///<summary>
        ///Returns all Routes and their metadata  + Jobs/Points for a given date
        ///</summary>
        [TestMethod]
        public void GetRoutesForDate_v1()
        {
            try
            {
                API api = new API(
                    _guid: My.m_guid,
                    _username: My.m_username,
                    _password: My.m_password
                    );

                wsResponse resp =
                    api.GetRoutesForDate_v1(
                    _routeDate: new DateTime(2016, 5, 31)
                    );

                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.resultCodeEnum == resultCodeEnum.RETURN_OK);
                Common_UnitTest.Log(resp);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        ///<summary>
        ///Returns all Unassigned jobs and their metadata  + points for a given date
        ///</summary>
        [TestMethod]
        public void GetUnassignedJobsForDate_v1()
        {
            try
            {
                API api = new API(
                    _guid: My.m_guid,
                    _username: My.m_username,
                    _password: My.m_password
                    );

                wsResponse_wsRouteVisitList resp =
                    api.GetUnassignedJobsForDate_v1(
                    _routeDate: new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)
                    );

                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.resultCodeEnum == resultCodeEnum.RETURN_OK);
                Common_UnitTest.Log(resp);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        ///<summary>
        ///Add Routes with Jobs using the syncronize method
        ///</summary>
        [TestMethod]        
        public void AddRoutesForDate_v3()
        {
            try
            {
                API api = new API(
                    _guid: My.m_guid,
                    _username: My.m_username,
                    _password: My.m_password
                    );

                List<wsRoute_v2> list = new List<wsRoute_v2>();
                //► Create 3 Routes
                for (int i = 0; i < 3; i++)
                {
                    wsRoute_v2 item = new wsRoute_v2();
                    item.RouteName = "Route Name " + i.ToString();                    
                    item.RouteDescription = "Route Description " + i.ToString();
                    item.StartTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 30, 0);
                    item.Worker = My.m_username; //assign to test_user                 
                    
                    //► Create/select StartPoint
                    Point s_p =  new Point();
                    s_p.CustomerId = "Test StartPoint";
                    s_p.Name = "Test Name StartPoint";
                    s_p.Address = "Test Address StartPoint";
                    s_p.Description = "Test Description StartPoint";
                    s_p.GeocodeX = 30.1;
                    s_p.GeocodeY = 30.2;
                    s_p.Tag1 = "Test Tag1 StartPoint";
                    s_p.Tag2 = "Test Tag2 StartPoint";
                    s_p.Tag3 = "Test Tag3 StartPoint";
                    s_p.Tag4 = "Test Tag4 StartPoint";
                    s_p.Tag5 = "Test Tag5 StartPoint";
                    s_p.Tag6 = "Test Tag6 StartPoint";
                    s_p.Tag7 = "Test Tag7 StartPoint";
                    s_p.Tag8 = "Test Tag8 StartPoint";
                    s_p.Tag9 = "Test Tag9 StartPoint";
                    s_p.Tag10 = "Test Tag10 StartPoint";
                    s_p.Visible = true;

                    item.StartPoint = s_p;


                    List<wsRouteVisit> rv_list = new List<wsRouteVisit>();
                    //► Create 3 visits
                    for (int y = 0; y < 3; y++)
                    {                       
                        Point p = new Point();
                        p.CustomerId = "Test CustomerId " + i.ToString() + " " + y.ToString();
                        p.Name = "Test Name " + i.ToString() + " " + y.ToString();
                        p.Address = "Test Address " + i.ToString() + " " + y.ToString();
                        p.Description = "Test Description " + i.ToString() + " " + y.ToString();
                        p.GeocodeX = 30.1;
                        p.GeocodeY = 30.2;
                        p.Tag1 = "Test Tag1 - " + i.ToString() + " " + y.ToString();
                        p.Tag2 = "Test Tag2 - " + i.ToString() + " " + y.ToString();
                        p.Tag3 = "Test Tag3 - " + i.ToString() + " " + y.ToString();
                        p.Tag4 = "Test Tag4 - " + i.ToString() + " " + y.ToString();
                        p.Tag5 = "Test Tag5 - " + i.ToString() + " " + y.ToString();
                        p.Tag6 = "Test Tag6 - " + i.ToString() + " " + y.ToString();
                        p.Tag7 = "Test Tag7 - " + i.ToString() + " " + y.ToString();
                        p.Tag8 = "Test Tag8 - " + i.ToString() + " " + y.ToString();
                        p.Tag9 = "Test Tag9 - " + i.ToString() + " " + y.ToString();
                        p.Tag10 = "Test Tag10 - " + i.ToString() + " " + y.ToString();
                        p.Visible = true;

                        Node n = new Node();
                        n.Name = "Job Name " + i.ToString() + " " + y.ToString();
                        n.Description = "Job Description " + i.ToString() + " " + y.ToString();
                        n.PlannedTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 11, 30, 0);
                        n.TimeFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 0, 0);
                        n.TimeTo = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);
                        n.Note = "Job Note " + i.ToString() + " " + y.ToString();
                        n.MaxDeliveryDate = DateTime.Now.AddMonths(1);
                        n.Tag1 = "Test Tag1 - " + i.ToString() + " " + y.ToString();
                        n.Tag2 = "Test Tag2 - " + i.ToString() + " " + y.ToString();
                        n.Tag3 = "Test Tag3 - " + i.ToString() + " " + y.ToString();
                        n.Tag4 = "Test Tag4 - " + i.ToString() + " " + y.ToString();
                        n.Tag5 = "Test Tag5 - " + i.ToString() + " " + y.ToString();
                        n.Tag6 = "Test Tag6 - " + i.ToString() + " " + y.ToString();
                        n.Tag7 = "Test Tag7 - " + i.ToString() + " " + y.ToString();
                        n.Tag8 = "Test Tag8 - " + i.ToString() + " " + y.ToString();
                        n.Tag9 = "Test Tag9 - " + i.ToString() + " " + y.ToString();
                        n.Tag10 = "Test Tag10 - " + i.ToString() + " " + y.ToString();
                        //n.Actions = "{\"yo\":1}";                        
                        wsRouteVisit rv = new wsRouteVisit();
                        rv.Point = p;
                        rv.Node = n;

                        rv_list.Add(rv);
                    }

                    item.RouteVisits = rv_list.ToArray();                                    
                    list.Add(item);
                }
                           
                wsResponse resp =
                    api.AddRoutesForDate_v3(
                    _routeDate : DateTime.Now,
                    _bridgeServiceMode : bridgeServiceModeEnum.syncronize,
                    _bUpdatePointLatLng : false,
                    _routelist: list.ToArray()    
                    );

                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.resultCodeEnum == resultCodeEnum.RETURN_OK);
                Common_UnitTest.Log(resp);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        ///<summary>
        ///Add unassigned jobs using the syncronize method with AddRoutesForDate_v3
        ///</summary>
        [TestMethod]      
        public void AddRoutesForDate_v3_EXAMPLE_UNASSIGNED_JOBS()
        {
            try
            {
                API api = new API(
                    _guid: My.m_guid,
                    _username: My.m_username,
                    _password: My.m_password
                    );

                List<wsRoute_v2> list = new List<wsRoute_v2>();
                //► Create 3 Routes

                wsRoute_v2 item = new wsRoute_v2();

                //->IN ORDER TO SEND UNASSIGNED JOBS ROUTE NAME MUST BE NULL

                item.RouteName = null;

                List<wsRouteVisit> rv_list = new List<wsRouteVisit>();
                //► Create 3 visits
                for (int i = 0; i < 3; i++)
                {
                    Point p = new Point();
                    p.CustomerId = "Test CustomerId " + i.ToString();
                    p.Name = "Test Name " + i.ToString();
                    p.Address = "Test Address " + i.ToString();
                    p.Description = "Test Description " + i.ToString();
                    p.GeocodeX = 30.1;
                    p.GeocodeY = 30.2;
                    p.Tag1 = "Test Tag1 - " + i.ToString();
                    p.Tag2 = "Test Tag2 - " + i.ToString();
                    p.Tag3 = "Test Tag3 - " + i.ToString();
                    p.Tag4 = "Test Tag4 - " + i.ToString();
                    p.Tag5 = "Test Tag5 - " + i.ToString();
                    p.Tag6 = "Test Tag6 - " + i.ToString();
                    p.Tag7 = "Test Tag7 - " + i.ToString();
                    p.Tag8 = "Test Tag8 - " + i.ToString();
                    p.Tag9 = "Test Tag9 - " + i.ToString();
                    p.Tag10 = "Test Tag10 - " + i.ToString();
                    p.Visible = true;

                    Node n = new Node();
                    n.Name = "Job Name " + i.ToString();
                    n.Description = "Job Description " + i.ToString();
                    n.PlannedTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 11, 30, 0);
                    n.TimeFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 0, 0);
                    n.TimeTo = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);
                    n.Note = "Job Note " + i.ToString();
                    n.MaxDeliveryDate = DateTime.Now.AddMonths(1);
                    n.Tag1 = "Test Tag1 - " + i.ToString();
                    n.Tag2 = "Test Tag2 - " + i.ToString();
                    n.Tag3 = "Test Tag3 - " + i.ToString();
                    n.Tag4 = "Test Tag4 - " + i.ToString();
                    n.Tag5 = "Test Tag5 - " + i.ToString();
                    n.Tag6 = "Test Tag6 - " + i.ToString();
                    n.Tag7 = "Test Tag7 - " + i.ToString();
                    n.Tag8 = "Test Tag8 - " + i.ToString();
                    n.Tag9 = "Test Tag9 - " + i.ToString();
                    n.Tag10 = "Test Tag10 - " + i.ToString();
                    //n.Actions = "{\"yo\":1}";                        
                    wsRouteVisit rv = new wsRouteVisit();
                    rv.Point = p;
                    rv.Node = n;

                    rv_list.Add(rv);
                }

                item.RouteVisits = rv_list.ToArray();
                list.Add(item);


                wsResponse resp =
                    api.AddRoutesForDate_v3(
                    _routeDate: DateTime.Now,
                    _bridgeServiceMode: bridgeServiceModeEnum.syncronize,
                    _bUpdatePointLatLng: false,
                    _routelist: list.ToArray()
                    );

                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.resultCodeEnum == resultCodeEnum.RETURN_OK);
                Common_UnitTest.Log(resp);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }

}
